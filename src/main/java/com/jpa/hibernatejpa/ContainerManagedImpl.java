package com.jpa.hibernatejpa;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/*

 @Author melone
 @Date 7/7/18 
 
 */
@Repository
@Transactional
public class ContainerManagedImpl {
    @PersistenceContext
    private EntityManager entityManager;

    public Employe viewAllEmploye(){
        if(entityManager!=null) {
            return entityManager.find(Employe.class, 1);
        }else {
            return new Employe("ram", 12000);
        }
    }
}
