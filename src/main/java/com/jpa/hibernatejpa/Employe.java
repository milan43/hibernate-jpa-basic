package com.jpa.hibernatejpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*

 @Author melone
 @Date 7/6/18 
 
 */
@Entity
public class Employe {
    @Id
    private int e_id;
    private String e_name;
    private double e_salary;

    public Employe(){}

    public Employe(String e_name, double e_salary) {
        this.e_name = e_name;
        this.e_salary = e_salary;
    }

    public int getE_id() {
        return e_id;
    }

    public void setE_id(int e_id) {
        this.e_id = e_id;
    }

    public String getE_name() {
        return e_name;
    }

    public void setE_name(String e_name) {
        this.e_name = e_name;
    }

    public double getE_salary() {
        return e_salary;
    }

    public void setE_salary(double e_salary) {
        this.e_salary = e_salary;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "e_id=" + e_id +
                ", e_name='" + e_name + '\'' +
                ", e_salary=" + e_salary +
                '}';
    }
}
