package com.jpa.hibernatejpa;

import java.util.List;

/*

 @Author melone
 @Date 7/6/18 
 
 */
public interface EmployeeDao {
    void insert(Employe employe);
    List<Employe> viewAll();
    Employe getById(int id);
    void removeEmp(int id);
    void update(String name, int id);
}
