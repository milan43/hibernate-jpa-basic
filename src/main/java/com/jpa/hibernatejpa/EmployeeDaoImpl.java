package com.jpa.hibernatejpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/*

 @Author melone
 @Date 7/6/18 
 
 */
public class EmployeeDaoImpl implements EmployeeDao {
    EntityManager entityManager;

    public void getManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("pu");
        this.entityManager = factory.createEntityManager();
    }

    public void insert(Employe employe) {
        getManager();
        entityManager.getTransaction().begin();
        entityManager.persist(employe);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public List<Employe> viewAll() {
        getManager();
        entityManager.getTransaction().begin();
        return entityManager.createQuery("SELECT e FROM Employe e").getResultList();
    }

    public Employe getById(int id) {
        getManager();
        entityManager.getTransaction().begin();
        return entityManager.find(Employe.class, id);
    }

    public void removeEmp(int id) {
        getManager();
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from Employe where e_id=:id").setParameter("id", id).executeUpdate();
        entityManager.close();
    }

    public void update(String name, int id) {
        getManager();
        entityManager.getTransaction().begin();
        entityManager.createQuery("update Employe set e_name =:name where e_id=:id").setParameter("name",name).setParameter("id", id).executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
